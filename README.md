# AWS Secrets Manager CI Template Extension

This is not suitible for production usage. Use as a reference to build from!

## CI/CD Process

The CI/CD process is defined in the `.gitlab-ci.yml` file, which tells the GitLab runner what to do at each stage of the pipeline.

### Stages

1. **prepare**: This stage retrieves AWS secrets using the included configuration files.
2. **build**: This stage builds the application by running the `echo "Building application..."` script.
3. **deploy**: This stage deploys the application by running the `echo "Deploying application..."` script.

### Docker Image

The pipeline does not specify a Docker image, so it likely uses the default image provided by the GitLab runner.

### Configuration Files

The `.gitlab-ci.yml` file includes two configuration files:

- `.gitlab/ci/aws_secrets_retrieve_many.yml`: This file is included in the `build` stage and likely contains configuration for retrieving multiple AWS secrets.
- `.gitlab/ci/aws_secrets_retrieve_one.yml`: This file is included in the `deploy` stage and likely contains configuration for retrieving a single AWS secret.

### Jobs

1. **build_job**: This job is part of the `build` stage and extends the `.aws_secrets_retrieve_many` configuration. It runs the `echo "Building application..."` script.
2. **deploy_job**: This job is part of the `deploy` stage and extends the `.aws_secrets_retrieve_one` configuration. It runs the `echo "Deploying application..."` script.

## Variables

No variables are defined in this configuration file.

## Adding to Your Project

To use this CI/CD configuration for your own project, copy the contents of `.gitlab-ci.yml` into a new file in the root of your repository.

Then, customize the file as needed, adding any additional stages, scripts, or configuration required for your project.
